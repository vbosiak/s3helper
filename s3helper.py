#!/usr/bin/python3
import ConfigManager

import boto.s3.connection
import boto3
from botocore.exceptions import ClientError
from colored import fg, attr
from datetime import datetime
from tabulate import tabulate
import sys
import os

CONFIG = ConfigManager.load_config()
BUCKET = CONFIG['default_bucket']

conn = boto.connect_s3()
s3 = boto3.resource('s3')


def cli():
    os.system('clear')
    response = -1

    while response != 0:
        print(f"\nCurrent bucket: %s{BUCKET}%s\n" % (fg(3), attr(0)))
        print("1. List all buckets\n"
              f"2. List files in {BUCKET}\n"
              "3. Change current bucket\n"
              f"4. Remove some file from {BUCKET}\n"
              "5. Remove all files created before some date\n"
              "6. Upload file\n"
              "7. Download file\n\n"
              "0. Exit")

        response = int(input("> "))

        if response == 1:
            show_buckets()
        elif response == 2:
            show_files()
        elif response == 3:
            change_bucket()
        elif response == 4:
            remove_file()
            show_files()
        elif response == 5:
            remove_all_files_before_date()
            show_files()
        elif response == 6:
            upload_file()
        elif response == 7:
            download_file()
        elif response == 0:
            exit(0)


def show_buckets():
    table = []
    print("Buckets:")
    for bucket in conn.get_all_buckets():
        table.append([bucket.name, bucket.creation_date])
    print(tabulate(table, headers=["#", "Name", "Created"], showindex="always", tablefmt="github"))


def show_files():
    size = 0
    table = []
    print("\nFiles in {}:".format(BUCKET))
    for key in conn.get_bucket(BUCKET):
        table.append([key.last_modified, key.name, str(round(key.size / 1024, 3)) + "KB"])
        size += key.size
    table.sort(key=lambda x: x[0])
    print(tabulate(table, headers=["#", "Created", "Name", "Size"], showindex="always", tablefmt="github"))

    print('\nBucket size: {}GB, {} items\n'.format(round(size / 1e9, 3), len(table)))


def change_bucket():
    global BUCKET

    BUCKET = input('Bucket name: ')
    ConfigManager.set_default_bucket(BUCKET)


def remove_file():
    file = input("File name: ")

    conn.get_bucket(BUCKET).delete_key(file)


def remove_all_files_before_date():
    def in_excluded(name):
        return "samples" in name or "empty.txt.zip" in name or "vmv-test" in name

    date = input("Date [yyyy-mm-dd]: ")
    date = datetime.strptime(date + "T00:00:01", "%Y-%m-%dT%H:%M:%S")

    try:
        for key in conn.get_bucket(BUCKET):
            file_date = datetime.strptime(key.last_modified[:-5], "%Y-%m-%dT%H:%M:%S")
            if not in_excluded(key.name) and file_date < date:
                conn.get_bucket(BUCKET).delete_key(key.name)
        print('SUCCESS')
    except ValueError as err:
        print("[ERROR] ValueError: {0}".format(err))
    except:
        print("[ERROR] Unexpected error:", sys.exc_info()[0])
        raise


def upload_file():
    file_with_path = input('Filename with full path: ').strip()
    key_name = input('New S3 key name: ').strip()

    obj = conn.get_bucket(BUCKET).new_key(key_name)
    obj.set_contents_from_filename(file_with_path)


def download_file():
    s3object = input("S3 Object name: ").strip()
    local_file = input("Save as: ")

    try:
        s3.Bucket(BUCKET).download_file(s3object, local_file)
        print('SUCCESS\n')
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("[ERROR] The object does not exist.\n")
        else:
            raise
    except:
        print("[ERROR] Unexpected error:", sys.exc_info()[0])
        raise


cli()
