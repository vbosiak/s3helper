import os
import json
from pathlib import Path

CONFIG_DIRECTORY = str(Path.home()) + '/.config/s3helper'
CONFIG_PATH = CONFIG_DIRECTORY + '/config.json'


def load_config():
    if os.path.exists(CONFIG_PATH):
        file = open(CONFIG_PATH, 'r')
        config = json.loads(file.read())
        file.close()
        return config
    else:
        os.mkdir(CONFIG_DIRECTORY)
        file = open(CONFIG_PATH, 'w')
        file.write(json.dumps({"default_bucket": "test"}))
        file.close()
        return load_config()


def set_default_bucket(bucket):
    config = load_config()
    config['default_bucket'] = bucket
    file = open(CONFIG_PATH, 'w')
    file.write(json.dumps(config))
    file.close()

