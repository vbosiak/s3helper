# s3helper

Amazon S3 helper script. Written on Python 3

## Installing dependencies

```sh
sudo apt update
sudo apt install -y python3-pip

pip3 install boto boto3 colored prettytable
``` 

## Usage

```sh
python3 ./s3helper.py
```
